package com.chaoscrasher.bedwars.runtimemanagement;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.chaoscrasher.bedwars.data.ActiveGame;
import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;

public class RuntimeGamesManagement 
{
	private static List<Player> matchmakingQueue = new ArrayList();
	
	private static ActiveGame priorityGameInstance;
	
	private static HashMap<String, BedwarsArena> allArenas = new HashMap<String, BedwarsArena>();
	private static HashMap<String, ActiveGame> arenasInAction = new HashMap<String, ActiveGame>();
	
	public static void addToAllArenas(BedwarsArena bwa)
	{
		allArenas.put(bwa.getName(), bwa);
	}
	
	public static boolean removeArenaFromAll(String aname)
	{
		return (allArenas.remove(aname) != null);
	}
	
	public static int removeArenaInAction(String aname)
	{
		ActiveGame ag = arenasInAction.get(aname);
		if (ag != null)
		{	
			if (ag.isReadyForRemoval())
			{
				arenasInAction.remove(aname);
				System.out.println("Arena removed successfully");
				return 0;
			}
			System.out.println("Arena is not ready for removal.");
			return -2;
		}
		System.out.println("Are is not even integrated into arenasInAction");
		return -1;
	}

	public static HashMap<String, BedwarsArena> getAllArenas() 
	{
		return allArenas;
	}

	public static HashMap<String, ActiveGame> getArenasInAction() 
	{
		return arenasInAction;
	}
	
	public static boolean isArenaInAction(String arena)
	{
		return arenasInAction.containsKey(arena);
	}
	
		public static boolean isArenaInAction(BedwarsArena arena)
		{
			return arenasInAction.containsKey(arena.getName());
		}
	
	public static boolean isArenaIntegratedIntoAll(BedwarsArena bwa)
	{
		return allArenas.containsValue(bwa);
	}
	
	public static boolean isArenaIntegratedIntoAll(String name)
	{
		return allArenas.containsKey(name);
	}
	
	public static BedwarsArena getArenaFromAll(String name)
	{
		return allArenas.get(name);
	}
	
	public static boolean addPlayerToQueue(Player player)
	{
		if (!isPlayerInQueue(player))
		{
			matchmakingQueue.add(player);
			updateMatchmaking();
			return true;
		}
		return false;
	}
	
	public static List<Player> getPlayersInQueue()
	{
		return matchmakingQueue;
	}
	
	public static boolean isPlayerInQueue(Player p)
	{
		return matchmakingQueue.contains(p);
	}
	
	public static void removePlayerFromSystem(Player player)
	{
		matchmakingQueue.remove(player);
		if (priorityGameInstance != null && priorityGameInstance.getAllPlayers().contains(player)) priorityGameInstance.removePlayer(player, false);
		else
		{
			List<String> bwd = BedwarsDataManagement.getActiveGameData(player);	
			if (bwd != null) {getActiveGame(bwd.get(0)).removePlayer(player, true);}
		}
	}
	
	public static BedwarsArena getArenaOfLocation_ignore_y(Location loc)
	{
		for (String bwa: allArenas.keySet())
		{
			BedwarsArena arena = getArenaFromAll(bwa);
			if (UtilityFunctions.isABetweenBAndC_ignore_y(loc, arena.getArenaStart(), arena.getArenaEnd()))
			{
				return arena;
			}
		}
		return null;
	}
	
	public static BedwarsArena getArenaOfLocation(Location loc)
	{
		for (String bwa: allArenas.keySet())
		{
			BedwarsArena arena = getArenaFromAll(bwa);
			if (UtilityFunctions.isABetweenBAndC(loc, arena.getArenaStart(), arena.getArenaEnd()))
			{
				return arena;
			}
		}
		return null;
	}
	
	public static List<String> getNonTiedUpValidArenas()
	{
		Set<String> set = allArenas.keySet();
		List<String> list = new ArrayList();
		list.addAll(set); String[] bwas = new String[list.size()]; bwas = list.toArray(bwas);
		
		for (String bwa: bwas)
		{
			if (!getArenaFromAll(bwa).readyForMM())
			{
				list.remove(bwa);
			}
		}
		return list;
	}
	
	public static ActiveGame getActiveGame(String name)
	{
		return arenasInAction.get(name);
	}
	
	public static void updateMatchmaking()
	{
		for (ActiveGame ag: arenasInAction.values())
		{
			if (ag.isReadyForRemoval())
			{
				arenasInAction.remove(ag);
			}
		}
		
		if (priorityGameInstance == null)
		{
			System.out.println("No exiting priority game instance! Choosing new...");
			if (chooseNextPriorityInstance())
			updateMatchmaking();
		}
		else if (priorityGameInstance.hasEnoughPlayers() && !priorityGameInstance.isStarted())
		{
			System.out.println("Starting new Game with arena "+priorityGameInstance.getTargetArena().getName());
			priorityGameInstance.start();
			arenasInAction.put(priorityGameInstance.getName(), priorityGameInstance);
			if (chooseNextPriorityInstance())				
			updateMatchmaking();
		}
		else
		{
			if (!matchmakingQueue.isEmpty())
			{
				System.out.println("There is an existing priority map but it doesn't have enough players. Adding players from queue. There are "+matchmakingQueue.size()+" possible players");
				int suc = priorityGameInstance.addPlayers(matchmakingQueue);
				System.out.println("We were able to add "+suc+" players! Removing them from the matchmaking queue now!");
				for (int i = 0; i < suc; i++)
				{
					matchmakingQueue.remove(matchmakingQueue.size()-1);
				}
				System.out.println("Restarting matchmaking!");
				updateMatchmaking();
			}
			else
			{
				System.out.println("There is an existing priority map but it doesn't have enough players and there are no players in queue. Aborting until next cycle.");
			}
		}
	}
		
		private static boolean chooseNextPriorityInstance()
		{
			if (priorityGameInstance == null || priorityGameInstance.isStarted())
			{
				Random random = new Random();
				List<String> arenas = getNonTiedUpValidArenas(); 
				
				if (arenas.size() > 0)
				{
					int r = random.nextInt(arenas.size());
					priorityGameInstance = new ActiveGame(getArenaFromAll(arenas.get(r)));
					System.out.println("New priority game instance was choosen!");
					return true;
				}
				else
				{
					System.err.println("There is no valid GameMap...");
					return false;
				}
			}
			return false;
		}
		
	public static ActiveGame getPriorityInstance()
	{
		return priorityGameInstance;
	}
		
	public static void printArenas()
	{
		System.out.println("\n("+allArenas.size()+") all arenas: ");
		for (String name: allArenas.keySet())
		{
			System.out.println(allArenas.get(name));
		}
		System.out.println("\n("+arenasInAction.size()+") active games: ");
		for (String name: arenasInAction.keySet())
		{
			System.out.println(name);
		}
	}
	
	public static void saveFlatFile() throws IOException
	{
		FileOutputStream fos = new FileOutputStream("amap.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(allArenas);
        oos.close();
	}
	
	public static void loadFlatFile() throws IOException, ClassNotFoundException
	{
		try{
		FileInputStream fis = new FileInputStream("amap.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		allArenas = (HashMap<String, BedwarsArena>) ois.readObject();
		ois.close();
		}
		catch (FileNotFoundException e){System.err.println("Couldn't find apam.ser");}
	}
}
