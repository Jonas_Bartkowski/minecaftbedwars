package com.chaoscrasher.bedwars.main;

import static com.chaoscrasher.spigotutility.all.commandexecutors.CommandExcecutorFunctions.*;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;

import net.md_5.bungee.api.ChatColor;

public class BedwarsPlayerCommandExcecutor implements CommandExecutor
{
	public static boolean DEBUG = false;
	
	public static final String ONLY_PLAYER_ERROR = ChatColor.RED+"Only a player can do this.";
	public static final String STD_JOIN_CMD = "join";
	public static final String STD_QUIT_CMD = "quit";

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		sender.sendMessage("Got your p command");
		if (args.length == 1)
		{
			if (args[0].equalsIgnoreCase(STD_QUIT_CMD))
			{
				if (sender instanceof Player)
				{
					Player player = (Player) sender;
				
					if (RuntimeGamesManagement.isPlayerInQueue(player))
					{
						sendGreen(player, "You were removed from the game.");
					}
					else if (BedwarsDataManagement.isPayerInGame(player))
					{
						sendOrange(player, "You were removed from your game..");
						if (DEBUG) System.out.println("Player "+player.getName()+" left his game.");
					}
					else
					{
						sendOrange(player, "You are neither in the queue, nor in a game.");
					}
					RuntimeGamesManagement.removePlayerFromSystem(player);
					return true;
				}
				else
				{
					sender.sendMessage(ONLY_PLAYER_ERROR);
				}
				return true;
			}
			else if (args[0].equalsIgnoreCase(STD_JOIN_CMD))
			{
				if (sender instanceof Player)
				{
					Player player = (Player) sender;
				
					if (RuntimeGamesManagement.isPlayerInQueue(player))
					{
						player.sendMessage(ChatColor.RED+"You are already in a game.");
					}
					else
					{
						RuntimeGamesManagement.addPlayerToQueue(player);
						player.sendMessage(ChatColor.GREEN+"You joined the queue!");
						System.out.println("Player "+player.getName()+" joined the queue.");
					}
					return true;
				}
				else
				{
					sender.sendMessage(ONLY_PLAYER_ERROR);
				}
				return true;
			}
			else if (args[0].equalsIgnoreCase("kit"))
			{
				if (sender instanceof Player)
				{
					Player player = (Player) sender;
					PlayerInventory inv = player.getInventory();
					
					ItemStack sword = new ItemStack(Material.STONE_SWORD);
					ItemStack armor = new ItemStack(Material.LEATHER_CHESTPLATE);
					
					if (!inv.contains(sword) && (!inv.contains(armor)))
					{
						if (!BedwarsDataManagement.hasPlayerGottenKit(player))
						{
							player.getInventory().addItem(sword, armor);
							BedwarsDataManagement.setPlayerGotKit(player, true);
						}
						else
						{
							player.sendMessage(ChatColor.GREEN+"You already got that.");
						}
					}
					else
					{
						player.sendMessage(ChatColor.RED+"You already have that.");
					}
				}
				else
				{
					sender.sendMessage(ONLY_PLAYER_ERROR);
				}
				return true;
			}
		}
		return false;
	}

}
