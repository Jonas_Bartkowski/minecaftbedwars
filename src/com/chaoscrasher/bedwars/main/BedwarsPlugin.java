package com.chaoscrasher.bedwars.main;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.chaoscrasher.bedwars.listeners.AdministrationListener;
import com.chaoscrasher.bedwars.listeners.IngameListener;
import com.chaoscrasher.bedwars.listeners.OusideGameListener;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.spigotutility.all.commandexecutors.CommonCommandsExcecutor;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;
import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;
import com.chaoscrasher.spigotutility.all.listeners.GeneralListener;

import net.md_5.bungee.api.ChatColor;

public class BedwarsPlugin extends JavaPlugin
{
	public static BedwarsPlugin ref;
	
	@Override
	public void onEnable()
	{
		ref = this;
		System.out.println("Registering commands.");
		this.getCommand("bwa").setExecutor(new BedwarsAdminCommandExcecutor());
		this.getCommand("bw").setExecutor(new BedwarsPlayerCommandExcecutor());
		this.getCommand("ccd").setExecutor(new CommonCommandsExcecutor());
		this.getServer().getPluginManager().registerEvents(new AdministrationListener(), this);
		this.getServer().getPluginManager().registerEvents(new IngameListener(), this);
		this.getServer().getPluginManager().registerEvents(new OusideGameListener(), this);	
		this.getServer().getPluginManager().registerEvents(new GeneralListener(), this);
		
		loadAll();
		PlayerDataManagement.clearAll();
	}
	
	@Override
	public void onDisable()
	{
		saveAll();
	}
	
	public static boolean saveAll()
	{
		try 
		{
			ServerDataManagement.save();
			
			PlayerDataManagement.saveFlatFile();
			System.out.println(ChatColor.GREEN+"saving of player data complete");
			
			RuntimeGamesManagement.saveFlatFile();
			System.out.println(ChatColor.GREEN+"saving of arena data complete");
			return true;
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean loadAll()
	{
		try
		{
			PlayerDataManagement.loadFlatFile();
			System.out.println(ChatColor.GREEN+"Loading of player data complete");
			
			RuntimeGamesManagement.loadFlatFile();
			System.out.println(ChatColor.GREEN+"Loading of arena data complete");
			
			ServerDataManagement.supplyConfig(ref.getConfig());
			ServerDataManagement.load();
			return true;
		} catch (ClassNotFoundException | IOException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
