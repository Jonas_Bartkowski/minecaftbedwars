package com.chaoscrasher.bedwars.main;

import static com.chaoscrasher.bedwars.data.CONSTANTS.PLACE_ARENA_JOIN_SIGN_VAL;

import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_ARENA_END_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_ARENA_START_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_BED_LOCATION_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_MAX_PLAYERS_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_MIN_PLAYERS_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_NUM_TEAMS_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_POST_GAME_SPAWN_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_TEAM_SPAWN_VAL;
import static com.chaoscrasher.spigotutility.all.commandexecutors.CommandExcecutorFunctions.*;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.material.Bed;

import com.chaoscrasher.bedwars.data.ActiveGame;
import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.spigotutility.all.data.BedLocation;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;
import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;

import net.md_5.bungee.api.ChatColor;

public class BedwarsAdminCommandExcecutor implements CommandExecutor
{
	public static final String EDIT_ARENA_CMD = "edit";
	public static final String TP_ARENA_CMD = "tp";
	public static final String DEL_ARENA_CMD = "delete";
	public static final String SET_POST_GAME_ENDPOINT_CMD = "setpge";
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if (sender.isOp())
		{
			sender.sendMessage("Got your a command..: args.length == "+args.length);
			if (args.length == 1)
			{ 
				if (args[0].equalsIgnoreCase("lbs") && sender instanceof Player)
				{
					Player player = (Player) sender;
					player.sendMessage(player.getBedSpawnLocation()+"");
					return true;
				}
			    else if (args[0].equalsIgnoreCase("lps"))
				{
					PlayerDataManagement.printMap();
					return true;
				}
				else if (args[0].equalsIgnoreCase("las"))
				{
					RuntimeGamesManagement.printArenas();
					if (sender instanceof Player)
					{
						Set<String> arenas = RuntimeGamesManagement.getAllArenas().keySet();
						if (arenas != null)
						{
							sender.sendMessage("Found "+arenas.size()+" arenas.");
							for (String arena: arenas)
							{
								sender.sendMessage(RuntimeGamesManagement.getArenaFromAll(arena).toString()+"\n");
							}
						}
						else
						{
							sender.sendMessage("No arenas found.");
						}
					}
					return true;
				}
				else if (args[0].equalsIgnoreCase("lmmq"))
				{
					sender.sendMessage("Listing players in queue...");
					List<Player> ps = RuntimeGamesManagement.getPlayersInQueue();
					for (Player p: ps)
					{
						sender.sendMessage(p.getName());
					}
					
					sender.sendMessage("Listing players in priority instance...");
					ActiveGame ag = RuntimeGamesManagement.getPriorityInstance();
					if (ag != null)
					{
						for (Player p: ag.getAllPlayers())
						{
							sender.sendMessage(p.getName());
						}
					}
					return true;
				}	
				else if (args[0].equalsIgnoreCase("daa"))
				{
					sender.sendMessage(ChatColor.RED+"Deleting all arenas...");
					RuntimeGamesManagement.getAllArenas().clear();
					return true;
				}
				else if (args[0].equalsIgnoreCase("cmm"))
				{
					RuntimeGamesManagement.updateMatchmaking();
					sender.sendMessage("Matchmaking called...");
					return true;
				}
				else if (args[0].equalsIgnoreCase("casd"))
				{
					PlayerDataManagement.clearPlayerData((Player) sender);
					sender.sendMessage(ChatColor.GREEN+"Cleared all of your state data...");
					return true;
				}
				else if (args[0].equalsIgnoreCase("saveall"))
				{
					BedwarsPlugin.saveAll();
					sender.sendMessage("Saving all data...");
					return true;
				}
				else if
				(
			    args[0].equalsIgnoreCase(SET_ARENA_START_VAL) || args[0].equalsIgnoreCase(SET_ARENA_END_VAL) ||
				args[0].equalsIgnoreCase(SET_BED_LOCATION_VAL) || args[0].equalsIgnoreCase(SET_TEAM_SPAWN_VAL) ||
				args[0].equalsIgnoreCase(PLACE_ARENA_JOIN_SIGN_VAL)
				)
				{
					if (sender instanceof Player)
					{
						
						Player player = (Player) sender;					
						BedwarsArena arena = BedwarsDataManagement.getPlayerEditingArena_safe(player);
						//System.out.println(arena.size());
						if (arena != null)
						{
							String name = arena.getName();
							System.out.println("Player "+player.getName()+" is trying to edit arena "+name);							
							BedwarsArena bwa = RuntimeGamesManagement.getArenaFromAll(name);
							if (args[0].equalsIgnoreCase(SET_ARENA_START_VAL))
							{
								PlayerDataManagement.setNextHitApplies(player, true, false, SET_ARENA_START_VAL, "0");
								player.sendMessage(ChatColor.GREEN+"Your next hit will set the start point for arena '"+name+"'.");	
							}
							else if (args[0].equalsIgnoreCase(SET_ARENA_END_VAL))
							{ 
								PlayerDataManagement.setNextHitApplies(player, true, false, SET_ARENA_END_VAL, "0");
								player.sendMessage(ChatColor.GREEN+"Your next hit will set the end point for arena '"+name+"'.");
							}
							else if (args[0].equalsIgnoreCase(SET_BED_LOCATION_VAL))
							{
								PlayerDataManagement.setNextHitApplies(player, true, false, SET_BED_LOCATION_VAL,"0");
								List<BedLocation> bedLocs = arena.getTeamBedLocations();
								for (BedLocation bloc: bedLocs) 
								{
									if (bloc != null)
									{
										Location loc = bloc.getHeadLocation();							
										try
										{
											Location twinloc = UtilityFunctions.getTwinLocation(((Bed)loc.getBlock().getState().getData()), loc.getBlock());
											System.out.println("loc type: "+loc.getBlock().getType()+" twinloc type: "+twinloc.getBlock().getType());	
											twinloc.getBlock().setType(Material.AIR);
											loc.getBlock().setType(Material.AIR);
										}
										catch (ClassCastException e){}
									}
								}
								player.sendMessage(ChatColor.GREEN+"Existing beds reset. Your next hit will set the first of "+bwa.getNumerOfTeams()+" bed location for arena '"+name+"'.");
							}
							else if (args[0].equalsIgnoreCase(SET_TEAM_SPAWN_VAL))
							{
								PlayerDataManagement.setNextHitApplies(player, true, false, SET_TEAM_SPAWN_VAL, "0");
								player.sendMessage(ChatColor.GREEN+"Your next hit will set the first of "+bwa.getNumerOfTeams()+" team spawn point for arena '"+name+"'.");
							}
							else
							{
								return false;
							}
							/*
							else if (args[0].equalsIgnoreCase(PLACE_ARENA_JOIN_SIGN_VAL))
							{
								PlayerDataManagement.setNextHitApplies(player, PLACE_ARENA_JOIN_SIGN_VAL, true, false);
								player.sendMessage(ChatColor.GREEN+"Your next hit will set the next team spawn point for arena '"+arena.get(0)+"'.");
							}
							*/						
							return true;
						}
						else
						{
							sender.sendMessage(ChatColor.RED+"Sorry, but you are not currently editing an arena.");
							return true;
						}
					}
					else
					{
						sender.sendMessage("This only works for players.");
						return true;
					}
				}
			}
			else if (args.length == 2)
			{			
				if (args[0].equalsIgnoreCase(TP_ARENA_CMD))
				{
					if (sender instanceof Player)
					{
						Player player = (Player) sender;
						if (RuntimeGamesManagement.isArenaIntegratedIntoAll(args[1]))
						{
							if (!RuntimeGamesManagement.isArenaInAction(args[1]))
							{
								player.sendMessage(ChatColor.GREEN+"Teleporting you to arena "+args[1]+".");
								player.teleport(RuntimeGamesManagement.getArenaFromAll(args[1]).getArenaStart());
							}
							else
							{
								sendRed(player, "You cannot teleport to an arena that is in game...");
							}
						}
						else
						{
							sendRed(player, "Sorry, but this arena is not integrated into all.");
						}
					}
					else
					{
						onlyPlayersAllowed(sender);
					}
					return true;
				}			
				if (args[0].equalsIgnoreCase(EDIT_ARENA_CMD))
				{
					if (sender instanceof Player)
					{
						Player player = (Player) sender;	
						boolean success = BedwarsDataManagement.setPlayerEditArena_safe(player, args[1]);
						if (success)
						{
							player.sendMessage(ChatColor.GREEN+"You are now editing arena '"+args[1]+"'");
						}
						else
						{
							player.sendMessage("Sorry, arena '"+args[1]+"' does not exist.");
						}
						return true;					
					}
				}
				else if (args[0].equalsIgnoreCase(DEL_ARENA_CMD))
				{
					boolean rem = RuntimeGamesManagement.removeArenaFromAll(args[1]);
					if (rem)
					{
						sender.sendMessage(ChatColor.GREEN+"Arena '"+args[1]+"' was removed.");
					}
					else
					{
						sender.sendMessage(ChatColor.RED+"Arena '"+args[1]+"' doesn't exist.");
					}
					return true;
				}
				else if (args[0].equalsIgnoreCase(SET_NUM_TEAMS_VAL) || args[0].equalsIgnoreCase(SET_MAX_PLAYERS_VAL) || args[0].equalsIgnoreCase(SET_MIN_PLAYERS_VAL))
				{
					try
					{
						int num = Integer.valueOf(args[1]);	
						if (sender instanceof Player)
						{
							Player player = (Player) sender;										
							List<String> arenaName = PlayerDataManagement.getStateData(player, "editArena");
							if (arenaName != null)
							{
								if (RuntimeGamesManagement.isArenaIntegratedIntoAll(arenaName.get(0)))
								{
									BedwarsArena arena = RuntimeGamesManagement.getArenaFromAll(arenaName.get(0));
									if (args[0].equalsIgnoreCase("setNumTeams"))
									{
										arena.setNumberOfTeams(num);
										player.sendMessage("Arena '"+arenaName.get(0)+"' number of teams set to "+num+".");
									}										
									else if (args[0].equalsIgnoreCase("setMaxPlayers")) 
									{
										arena.setMaxPlayers(num);
										player.sendMessage("Arena '"+arenaName.get(0)+"' max player number set to "+num+".");
									}
									else if (args[0].equalsIgnoreCase("setMinPlayers"))
									{
										arena.setMinPlayersToStart(num);
										player.sendMessage("Arena '"+arenaName.get(0)+"' min player number set to "+num+".");
									}
									return true;															
								}
								else
								{
									player.sendMessage("Sorry, but there is no existing arena with the name'"+arenaName+"'.");
									return true;
								}
							}
							else
							{
								player.sendMessage("Sorry, but you have no arena set to edit");
								return true;
							}				
						}	
						else
						{
							sender.sendMessage(ChatColor.RED+"Sorry, but you need to be a player to do this.");
							return true;
						}				
					}
					catch (NumberFormatException e)
					{
						sender.sendMessage("'"+args[1]+"' must be a valid integer!");
						return true;
					}
				}
			}
			else if (args.length == 5)
			{
				if (args[0].equalsIgnoreCase("newArena"))
				{
					String name = args[1];
					try
					{					
						int teamSize = Integer.valueOf(args[2]);
						int maxPl = Integer.valueOf(args[3]);
						int minPl = Integer.valueOf(args[4]);
						
						RuntimeGamesManagement.addToAllArenas(new BedwarsArena(name, teamSize, maxPl, minPl));
						sender.sendMessage(ChatColor.GREEN+"Arena '"+name+"' was created with TS = "+teamSize+", MAXP = "+maxPl+" and MINP = "+minPl);
						
						if (sender instanceof Player)
						{
							Player player = (Player) sender;
							PlayerDataManagement.setStateData_quick(player, "editArena="+args[1]);
							player.sendMessage(ChatColor.GREEN+"You are now editing arena '"+args[1]+"'");
						}
						
						return true;
					}
					catch (NumberFormatException e)
					{
						sender.sendMessage(ChatColor.RED+"Sorry, but you didn't use 3 integers after newArena "+name+".");
						return true;
					}
				}
				
			}
			return false;
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"FORBIDDEN");
		}
		return true;
	}
}
