package com.chaoscrasher.bedwars.utility;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.chaoscrasher.bedwars.io.BedwarsDataManagement;

public class BedwarsUtilityFunctions 
{
	public static ItemStack[] STANDARD_KIT = new ItemStack[] {new ItemStack(Material.STONE_SWORD), new ItemStack(Material.LEATHER_CHESTPLATE), new ItemStack(Material.SANDSTONE, 64)};
	
	public static int givePlayerKit(Player player, boolean ignoreIfHas, boolean ignoreIfHad)
	{
		if (player != null)
		{
			if (ignoreIfHas)
			{
				for (ItemStack is: STANDARD_KIT)
				{
					if (player.getInventory().contains(is))
					{
						return 1;
					}
				}
			}
			else if (ignoreIfHad && BedwarsDataManagement.hasPlayerGottenKit(player))
			{
				
				return 2;
			}
			
			for (ItemStack is: STANDARD_KIT)
			{
				player.getInventory().addItem(is);
			}
			
			return 0;
		}
		return -1;
	}
}
