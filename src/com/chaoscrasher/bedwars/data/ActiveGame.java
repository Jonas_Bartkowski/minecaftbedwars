package com.chaoscrasher.bedwars.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.bedwars.utility.BedwarsUtilityFunctions;
import com.chaoscrasher.spigotutility.all.data.BedLocation;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;
import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;

import net.md_5.bungee.api.ChatColor;

public class ActiveGame 
{
	private BedwarsArena targetArena;
	private List<Player>[] playersOfTeams;
	private HashMap<Location, Material> editHistory = new HashMap<Location, Material>(); //notes original Material for every block that gets changed within the arena.
	
	private boolean started;
	private boolean finished;
	private boolean readyForRemoval;
	
	public ActiveGame(BedwarsArena targetArena) 
	{
		this.targetArena = targetArena.makeCopyOfInstance();
		this.playersOfTeams = new List[targetArena.getNumerOfTeams()];
		for (int i = 0; i < playersOfTeams.length; i++)
		{
			playersOfTeams[i] = new ArrayList();
		}
	}
		
	public boolean isStarted() 
	{
		return started;
	}

	public BedwarsArena getTargetArena() 
	{
		return targetArena;
	}

	public HashMap<Location, Material> getEditHistory() 
	{
		return editHistory;
	}
	
	public void onPlayerKillByPlayer(Player victim, Player killer)
	{
		System.out.println("Player ("+victim.getName()+") kill registered in active game of arena "+targetArena.getName()+"! Checking if team bed is intact!");
		BedwarsDataManagement.setPlayerGotKit(victim, false);
		if (!isTeamBedIntact(getPlayerTeam(victim)))
		{
			System.out.println("Bed is not intact... goodbye!");
			victim.sendMessage(ChatColor.RED+"You are DEAD and have no bed. Goodbye!");
			PlayerDataManagement.setRespawnLocation(victim, PlayerDataManagement.getPrePortLocation(victim), true);
			removePlayer(victim, true);
		}
		else
		{
			System.out.println("Bed is still intact...");
			System.out.println("I will respawn at "+victim.getBedSpawnLocation());
		}
	}
	
	public void onValidBedBreak(Location bed)
	{
		int teamNum = targetArena.getTeamNumberFromBedLocation(bed);
		System.out.println(teamNum+"'s bed was destroyed!");
		
		List<Player> saD = playersOfTeams[teamNum];
		Player[] sad = new Player[saD.size()];
		sad = saD.toArray(sad);
		
		for (Player player: sad)
		{
			player.sendMessage(ChatColor.RED+"Your bed has been broken. sad.");
			if (player.isDead())
			{
				System.out.println("Removing player due to being ded without bed");
				removePlayer(player, true);
				PlayerDataManagement.setRespawnLocation(player, PlayerDataManagement.getPrePortLocation(player), true);
				player.sendMessage(ChatColor.RED+"Since you are dead, this means you are hereby removed from the game.");
				if (isTeamDead(teamNum)){player.sendMessage(ChatColor.RED+"You were the last player. Your team has LOST the game. Good luck next time.");}
			}
			else
			{
				player.sendMessage(ChatColor.RED+"If you die now, you will be removed from the game!");
			}
		}
	}
	
	public boolean isTeamDead(int team)
	{
		return playersOfTeams[team].isEmpty();
	}
	
	public void doGameEndCheck()
	{
		System.out.println("Proceeding with game end check...");
		int lastNonDedTeamIndex = -1;
		int numDeadTeams = 0;
		
		for (int team = 0; team < playersOfTeams.length; team++)
		{
			if (isTeamDead(team))
			{
				numDeadTeams++;			
			}
			else
			{
				lastNonDedTeamIndex = team;
			}
		}
		System.out.println("Num dead teams: "+numDeadTeams);
		
		if (numDeadTeams == playersOfTeams.length-1)
		{
			System.out.println("Only one team left! Proceeding with crowning of victor...");
			List<Player> team = playersOfTeams[lastNonDedTeamIndex];
			for (Player player: team)
			{
				player.sendMessage(ChatColor.GREEN+"Your team won!");
			}
			
			finished = true;
			doCleanUp();
		}
	}
	
		public boolean hasPlayerBed(Player player)
		{
			return targetArena.getTeamBedLocations().get(getPlayerTeam(player)).getHeadLocation().getBlock().getType().equals(Material.BED);
		}
		
		public int getPlayerTeam(Player player)
		{
			int tindex = 0;
			for (List<Player> team: playersOfTeams)
			{
				if (team.contains(player))
					return tindex;
				
				tindex++;
			}
			return -1;
		}
		
		public boolean isTeamBedIntact(int team)
		{
			return targetArena.getTeamBedLocations().get(team).getHeadLocation().getBlock().getType().equals(Material.BED_BLOCK);
		}
		
	public void removePlayer(Player player, boolean doGameEndCheck)
	{	
		int team = getPlayerTeam(player);
		if (team != -1)
		{
			System.out.println("Team = "+team);
			playersOfTeams[team].remove(player);
			System.out.println("Team has only "+playersOfTeams[team].size()+" left.");
			if (doGameEndCheck) doGameEndCheck();
			BedwarsDataManagement.clearPlayerBedwarsData(player);
		}
	}
	
	public void start()
	{
		started = true;
		setDatabaseEntriesForPlayers();
		clearDroppedItems();
		teleportAllPlayersToStartLocations();		
		healthUpAllPlayer();
		outfitPlayers();
	}
	
		private void teleportAllPlayersToStartLocations()
		{
			int tindex = 0;
			for (List<Player> team: playersOfTeams)
			{
				for (Player player: team)
				{
					player.sendMessage(ChatColor.GREEN+"You are now being teleported to your spawn location and the game will begin!");
					player.teleport(targetArena.getTeamSpawnLocations().get(tindex));
					player.setGameMode(GameMode.SURVIVAL);			
					player.getInventory().clear();
				}
				tindex++;
			}
		}
		
		private void setDatabaseEntriesForPlayers()
		{
			int tind = 0;
			for (List<Player> team: playersOfTeams)
			{
				for (Player player: team)
				{
					BedwarsDataManagement.setBedwarsData(player, targetArena.getName(), tind);		
					
					Location tspawn = targetArena.getTeamSpawnLocations().get(tind);
					//System.out.println("Trying to set "+tspawn+" as spawn point for player "+player.getName());
					//System.out.println("Now spawn is = "+player.getBedSpawnLocation()+". players world is "+player.getLocation().getWorld());
					//player.setBedSpawnLocation(player.getLocation());
					//System.out.println("Now spawn is = "+player.getBedSpawnLocation());
					///PlayerDataManagement.setRespawnLocation(player, tspawn);
					PlayerDataManagement.setPrePortLocation(player, player.getLocation());
				}
				tind++;
			}
		}
		
		private void healthUpAllPlayer()
		{
			for (List<Player> team: playersOfTeams)
			{
				for (Player player: team)
				{
					UtilityFunctions.putPlayerIntoStandardCondition(player);
				}
			}
		}
		
		private void outfitPlayers()
		{
			for (List<Player> team: playersOfTeams)
			{
				for (Player player: team)
				{
					BedwarsUtilityFunctions.givePlayerKit(player, true, true);
				}
			}
		}
		
		private void clearDroppedItems()
		{
			UtilityFunctions.clearItems(targetArena.getArenaStart().getWorld(), targetArena.getArenaStart(), targetArena.getArenaEnd());
		}
	
	public List<Player> getAllPlayers()
	{
		List<Player> players = new ArrayList<>();
		for (List<Player> team: playersOfTeams)
		{
			players.addAll(team);
		}
		return players;
	}
	
	public Location getTeamSpawnOfPlayer(Player player)
	{
		int tind = getPlayerTeam(player);
		if (tind != -1)
		{
			return targetArena.getTeamSpawnLocations().get(tind);
		}
		return null;
	}
		
	public int addPlayers(List<Player> players)
	{
		players.remove(getAllPlayers());
		int couldjoinarena = this.targetArena.getMaxPlayers()-this.countPlayers();
		int couldjoinqueue = players.size();
		int peopleJoined = (couldjoinarena <= couldjoinqueue ? couldjoinarena: couldjoinqueue);
		if (peopleJoined <= 0) {return 0;}
		for (int i = 0; i < peopleJoined; i++)
		{
			int weakestTeamIndex = 0;
			int strengthOfWeakestTeam = playersOfTeams[0].size();
			for (int j = 1; j < playersOfTeams.length; j++) {if (playersOfTeams[j].size() < strengthOfWeakestTeam) {strengthOfWeakestTeam = playersOfTeams[j].size(); weakestTeamIndex = j;}}
			
			playersOfTeams[weakestTeamIndex].add(players.get(i));
			players.get(i).sendMessage(ChatColor.GREEN+"A game for you was found on map "+targetArena.getName()+" in team "+weakestTeamIndex+"!");
			players.get(i).sendMessage(ChatColor.YELLOW+"We still need "+howMuchMoreplayersNeededToJoin()+" more players before we can start!");
		}
		return peopleJoined;
	}
	
	public int countPlayers() 
	{
		int playNum = 0;
		for (List<Player> team: playersOfTeams)
		{
			playNum += team.size();
		}
		return playNum;
	}
	
	public String getName()
	{
		return targetArena.getName();
	}
	
	public boolean hasEnoughPlayers()
	{
		return countPlayers() >= targetArena.getMinPlayersToStart();
	}
	
	public int howMuchMoreplayersNeededToJoin()
	{
		return targetArena.getMinPlayersToStart()-countPlayers();
	}

	public boolean isFinished() 
	{
		return finished;
	}
	
	public boolean isReadyForRemoval() 
	{
		return readyForRemoval;
	}
	
	public void doCleanUp()
	{
		cleanUpPlayers();
		cleanUpMap();
		replaceBeds();
		this.readyForRemoval = true;
		System.out.println("Removing ActiveGame from arenasInAction. Previously there were "+RuntimeGamesManagement.getArenasInAction().size()+" Active Games.");
		RuntimeGamesManagement.removeArenaInAction(this.getName());
	}
	
		private void cleanUpPlayers()
		{
			//TODO: clear gameID and teamID entries of players
			
			List<Player> allPs = getAllPlayers();
			for (Player player: allPs)
			{
				removePlayer(player, false);
				Location ppLoc = PlayerDataManagement.getPrePortLocation(player);
				player.teleport(ppLoc != null ? ppLoc : ServerDataManagement.getServerSpawnPoint());
			}
		}
	
		private void cleanUpMap()
		{
			for (Location loc: editHistory.keySet())
			{
				loc.getBlock().setType(editHistory.get(loc));
			}
			
			targetArena.cleanUp();
		}
		
		private void replaceBeds()
		{
			for (BedLocation bedloc: this.targetArena.getTeamBedLocations())
			{
				UtilityFunctions.placeBed(bedloc.getHeadLocation(), bedloc.getFacing());
			}
		}
}
