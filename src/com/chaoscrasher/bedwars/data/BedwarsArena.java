package com.chaoscrasher.bedwars.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.utility.BedwarsUtilityFunctions;
import com.chaoscrasher.spigotutility.all.data.BedLocation;
import com.chaoscrasher.spigotutility.all.data.LocationString;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;

public class BedwarsArena implements Serializable
{
	private String name;
	
	private LocationString arenaStart; //y
	private LocationString arenaEnd; //y
	
	private int numberOfTeams; //y
	private int maxPlayers = -1; //y
	private int minPlayersToStart; //y
	
	private List<LocationString> teamSpawnLocations = new ArrayList(); //y	
	private List<BedLocation> teamBedLocations = new ArrayList(); //y
	//private List<Location> shopLocations = new ArrayList();  //not in beta
	
	public BedwarsArena(String name, int numteams, int maxplay, int minplay)
	{
		this.name = name;
		this.numberOfTeams = numteams;
		this.maxPlayers = maxplay;
		this.minPlayersToStart = minplay;
		for (int i = 0; i < numberOfTeams; i++)
		{
			teamBedLocations.add(null);
			teamSpawnLocations.add(null);
		}
	}
	
	public BedwarsArena(String name, Location arenaStart, Location arenaEnd, int numberOfTeams, int maxPlayers,
			int minPlayersToStart, List<Location> spawns) 
	{
		this(name, numberOfTeams, maxPlayers, minPlayersToStart);
		this.arenaStart = new LocationString(arenaStart);
		this.arenaEnd = new LocationString(arenaEnd);
		this.teamSpawnLocations = LocationString.locCollectionToLocStrList(spawns);
	}
	
	private BedwarsArena(String name, LocationString arenaStart, LocationString arenaEnd, int numberOfTeams, int maxPlayers,
			int minPlayersToStart, List<LocationString> spawns, List<BedLocation> teamBedLocations)
	{
		this(name, numberOfTeams, maxPlayers, minPlayersToStart);
		this.arenaStart = arenaStart; this.arenaEnd = arenaEnd; this.teamSpawnLocations = spawns; this.teamBedLocations = teamBedLocations;
	}



	public String getName() 
	{
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getArenaStart() {
		return arenaStart.getLocation();
	}

	public void setArenaStart(Location arenaStart) {
		this.arenaStart = new LocationString(arenaStart);
	}

	public Location getArenaEnd() {
		return arenaEnd.getLocation();
	}

	public void setArenaEnd(Location arenaEnd) {
		this.arenaEnd = new LocationString(arenaEnd);
	}

	public int getNumerOfTeams() {
		return numberOfTeams;
	}

	public void setNumberOfTeams(int teams) 
	{
		this.numberOfTeams = teams;
		if (minPlayersToStart < numberOfTeams)
		{
			minPlayersToStart = numberOfTeams;
		}
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public int getMinPlayersToStart() {
		return minPlayersToStart;
	}

	public void setMinPlayersToStart(int minPlayersToStart) 
	{
		this.minPlayersToStart = minPlayersToStart;
		if (this.numberOfTeams > minPlayersToStart)
		{
			this.numberOfTeams = minPlayersToStart;
		}
	}

	public List<Location> getTeamSpawnLocations() {
		return LocationString.locStrCollectionLocList(teamSpawnLocations);
	}

	public void setTeamSpawnLocations(List<Location> teamSpawnLocations) {
		this.teamSpawnLocations = LocationString.locCollectionToLocStrList(teamSpawnLocations);
	}
	
	public int getNumTeamSpawnLocations()
	{
		int num = 0;
		for (LocationString loc: teamSpawnLocations)
		{
			if (loc != null) num++;
		}
		return num;
	}
	
	public void setTeamSpawnLocation(int teamind, Location spawnLoc)
	{
		this.teamSpawnLocations.set(teamind, new LocationString(spawnLoc));
	}
	
	public void addTeamSpawnLocation(Location spawnLoc)
	{
		this.teamSpawnLocations.add(new LocationString(spawnLoc));
	}

	public List<BedLocation> getTeamBedLocations() {
		return teamBedLocations;
	}
	
	public void setTeamBedLocations(List<BedLocation> teamBedLocations) {
		this.teamBedLocations = teamBedLocations;
	}
	
	public int getNumTeamBedLocations()
	{
		int num = 0;
		for (BedLocation loc: teamBedLocations)
		{
			if (loc != null) num++;
		}
		return num;
	}
	
	public void setTeamBedLocation(int teamind, BedLocation bedLoc)
	{
		this.teamBedLocations.set(teamind, bedLoc);
	}
	
	public void addTeamBedLocation(BedLocation bedLoc)
	{
		this.teamBedLocations.add(bedLoc);
	}
	
	public boolean isLocationWithinArena(Location loc)
	{
		return UtilityFunctions.isABetweenBAndC_ignore_y(loc, arenaStart.getLocation(), arenaEnd.getLocation());
	}
	
	public int getTeamNumberFromBedLocation(Location location)
	{
		int tind = 0;
		for (BedLocation bloc: this.getTeamBedLocations())
		{
			if ((bloc.getHeadLocation() != null && bloc.getHeadLocation().equals(location)) || bloc.getFootLocation() != null && bloc.getFootLocation().equals(location))
			{
				return tind;
			}
			tind++;
		}
		return -1;
	}

	/*
	public List<Location> getShopLocations() {
		return shopLocations;
	}

	public void setShopLocations(List<Location> shopLocations) {
		this.shopLocations = shopLocations;
	}
	*/
	
	public boolean hasMinimumDataToBeUsed()
	{
		return (arenaStart != null && arenaEnd != null && numberOfTeams >=2 && maxPlayers >= numberOfTeams && minPlayersToStart >= numberOfTeams && teamSpawnLocations.size() == numberOfTeams && teamBedLocations.size() == numberOfTeams);
	}
	
	public BedwarsArena makeCopyOfInstance()
	{
		return new BedwarsArena(name, arenaStart, arenaEnd, numberOfTeams, maxPlayers, minPlayersToStart, teamSpawnLocations, teamBedLocations);
	}
	
	public boolean readyForMM()
	{
		boolean intts = (numberOfTeams > 1 && maxPlayers >= numberOfTeams && minPlayersToStart <= maxPlayers);
		return (intts && getNumTeamBedLocations() == numberOfTeams && getNumTeamSpawnLocations() == numberOfTeams);
	}
	
	public void cleanUp()
	{
		//System.out.println("Calling clean up...");
		
		int sx = arenaStart.getLocation().getBlockX(), sy = arenaStart.getLocation().getBlockY(), sz = arenaStart.getLocation().getBlockZ();
		int ex = arenaEnd.getLocation().getBlockX(), ey = arenaEnd.getLocation().getBlockY(), ez = arenaEnd.getLocation().getBlockZ();
		int vx = (ex - sx) / Math.abs(ex - sx), vy = (ey - sy) / Math.abs(ey - sy), vz = (ez - sz) / Math.abs(ez -sz);
		
		System.out.println("s(x="+sx+", y="+sy+",z="+sz+") e(x="+ex+", y="+ey+", z="+ez+") vx="+vx+" vy="+vy+" vz="+vz);
		
		for (int z = sz; z != ez; z += vz)
		{
			for (int y = sy; y != ey; y += vy)
			{
				for (int x = sx; x != ex; x += vx)
				{
					Location loc = new Location(arenaStart.getLocation().getWorld(), x, y, z);
					//System.out.println("Checking x="+" y="+y+" z="+z);
					if (BedwarsDataManagement.isMaterialAllowed(loc))
					{
						//System.out.println("will be reset!");
						loc.getBlock().setType(Material.AIR);
					}
				}
			}
		}
		
		//System.out.println("Clearing dropped items.");
	}
}
