package com.chaoscrasher.bedwars.data;

public class CONSTANTS 
{
	//administration values
	public static final String SET_POST_GAME_SPAWN_VAL = "setPostGameSpawn";
	
	
	public static final String SET_ARENA_START_VAL = "setas";
	public static final String SET_ARENA_END_VAL = "setae";
	public static final String SET_NUM_TEAMS_VAL = "setnt";
	public static final String SET_MAX_PLAYERS_VAL = "setmxp";
	public static final String SET_MIN_PLAYERS_VAL = "setmnp";
	
	public static final String SET_BED_LOCATION_VAL = "setbloc";
	public static final String SET_TEAM_SPAWN_VAL = "setsloc";
	
	public static final String PLACE_ARENA_JOIN_SIGN_VAL = "makeArenaJoinSign";
	
	//block data values
	public static final String IS_SHOP = "isShop";
	public static final String ARENA_START = "arenaStart:";
	public static final String ARENA_END = "arenaEnd:";
	public static final String TEAM_SPAWN = "teamSpawn:";
	public static final String BED_POSITION = "bedPosition:";
}
