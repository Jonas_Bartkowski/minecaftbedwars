package com.chaoscrasher.bedwars.listeners;

import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_ARENA_END_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_ARENA_START_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_BED_LOCATION_VAL;
import static com.chaoscrasher.bedwars.data.CONSTANTS.SET_TEAM_SPAWN_VAL;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.spigotutility.all.data.BedLocation;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;

import net.md_5.bungee.api.ChatColor;

public class AdministrationListener implements Listener
{
	@EventHandler
	public void onOpInteractWithBlock(PlayerInteractEvent e)
	{
		//e.getPlayer().sendMessage("You are in AdministrationListener");
		Player player = e.getPlayer();
		if (e.getPlayer().isOp())
		{
			//e.getPlayer().sendMessage("You are op");
			List<String> onh = PlayerDataManagement.getNextHitApplies(player);
			List<String> edit = PlayerDataManagement.getStateData(player, "editArena");
			
			if (edit != null)
			{
				e.getPlayer().sendMessage("You are editing arena "+edit.get(0)+".");
				String arena = edit.get(0);
				player.sendMessage("onh:"+onh.size());
				if (onh.size() == 2) player.sendMessage(onh.get(0)+onh.get(1));
				if (onh.size() == 1) player.sendMessage(onh.get(0));
				if (onh.contains(SET_ARENA_START_VAL))
				{
					player.sendMessage(ChatColor.GREEN+"Start of arena '"+arena+"' set to "+e.getClickedBlock().getLocation());
					RuntimeGamesManagement.getArenaFromAll(arena).setArenaStart(e.getClickedBlock().getLocation());
				}
				if (onh.contains(SET_ARENA_END_VAL))
				{
					player.sendMessage(ChatColor.GREEN+"End of arena '"+arena+"' set to "+e.getClickedBlock().getLocation());
					RuntimeGamesManagement.getArenaFromAll(arena).setArenaEnd(e.getClickedBlock().getLocation());
				}
				if (onh.contains(SET_BED_LOCATION_VAL))
				{
					int index = Integer.valueOf(onh.get(1));
					BedwarsArena bwa = RuntimeGamesManagement.getArenaFromAll(arena);
					BedLocation bl = new BedLocation(e.getClickedBlock().getLocation().add(0,1,0), UtilityFunctions.getPlayerFacing(player, true));
					//System.out.println(bl);
					bwa.setTeamBedLocation(index, bl);
					UtilityFunctions.placeBed(e.getClickedBlock().getLocation().add(0, 1, 0), bl.getFacing());
					//e.getClickedBlock().setType(Material.BED_BLOCK);
					//e.getClickedBlock().getLocation().add(1, 0, 0).getBlock().setType(Material.BED_BLOCK);
					
					player.sendMessage(ChatColor.GREEN+"bed #"+index+" of arena '"+arena+"' set to "+e.getClickedBlock().getLocation());
					if (index < bwa.getNumerOfTeams()-1){index++;}else{onh.clear(); player.sendMessage(ChatColor.GREEN+"All "+bwa.getNumerOfTeams()+" team beds were set!");}
					if (!onh.isEmpty()){onh.remove(1); onh.add(index+"");}
				}
				if (onh.contains(SET_TEAM_SPAWN_VAL))
				{
					int index = Integer.valueOf(onh.get(1));
					BedwarsArena bwa = RuntimeGamesManagement.getArenaFromAll(arena);
					bwa.setTeamSpawnLocation(index, e.getClickedBlock().getLocation());
					
					
					player.sendMessage(ChatColor.GREEN+"spawn #"+index+" of arena '"+arena+"' set to "+e.getClickedBlock().getLocation());
					if (index < bwa.getNumerOfTeams()-1){index++;}else{onh.clear(); player.sendMessage(ChatColor.GREEN+"All "+bwa.getNumerOfTeams()+" team spawns were set!");}
					if (!onh.isEmpty()){onh.remove(1); onh.add(index+"");}
				}
			}
		}
	}
}