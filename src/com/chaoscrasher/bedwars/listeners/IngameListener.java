package com.chaoscrasher.bedwars.listeners;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.material.Bed;

import com.chaoscrasher.bedwars.data.ActiveGame;
import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.bedwars.utility.BedwarsUtilityFunctions;
import com.chaoscrasher.spigotutility.all.functions.UtilityFunctions;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;
import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;

import net.md_5.bungee.api.ChatColor;

public class IngameListener implements Listener
{
	/*
	@EventHandler
	public void onShopClick(PlayerInteractEvent e)
	{
		if (BlockDataManagement.containsBlockData((e.getClickedBlock().getLocation()), IS_SHOP))
		{
			Inventory inv = Bukkit.createInventory(e.getPlayer(), 10);
			e.getPlayer().openInventory(inv);
		}
	}
	*/
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e)
	{
		Player player = e.getPlayer();
		List<String> bwd = BedwarsDataManagement.getActiveGameData(player);
		if (bwd != null)
		{
			ActiveGame ag = RuntimeGamesManagement.getActiveGame(bwd.get(0));
			e.setRespawnLocation(ag.getTeamSpawnOfPlayer(player));
			BedwarsUtilityFunctions.givePlayerKit(player, true, true);
		}
	}
	
	@EventHandler
	public void onFriendlyFire(EntityDamageByEntityEvent e)
	{
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player)
		{
			Player d = (Player) e.getDamager();
			Player v = (Player) e.getEntity();
			
			List<String> bwdd = BedwarsDataManagement.getActiveGameData(d);
			List<String> bwdv = BedwarsDataManagement.getActiveGameData(v);
			
			if (bwdd != null && bwdv != null && bwdd.get(0).equalsIgnoreCase(bwdv.get(0)) && bwdd.get(1).equalsIgnoreCase(bwdv.get(1)))
				e.setCancelled(true);
		}
		//TODO: block team killing
	}
	
	/*
	@EventHandler
	public void onBreakingItemSpawners(BlockBreakEvent e)
	{
		
	}
	*/
	
	@EventHandler
	public void onBedwarsKill(PlayerDeathEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getEntity());	
		if (bwd != null)
		{
			System.out.println("bwKill found! Checking for location ("+e.getEntity().getLocation()+")");
			if (RuntimeGamesManagement.getArenaFromAll(bwd.get(0)).isLocationWithinArena(e.getEntity().getLocation()))
			{
				System.out.println("Location is indeed within arena '"+bwd.get(0)+"'!");
				ActiveGame ag = RuntimeGamesManagement.getActiveGame(bwd.get(0));
				System.out.println(ag);
				ag.onPlayerKillByPlayer(e.getEntity(), e.getEntity().getKiller());
			}
		}
	}
	
	@EventHandler
	public void onArenaBreak(BlockBreakEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getPlayer());
		if (bwd != null)
		{
			BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getBlock().getLocation());			
			if (e.getBlock().getType().equals(Material.BED_BLOCK))
			{
				onBedBreak(e, bwd, bwa);
			}
			else if (!e.getBlock().getType().equals(Material.BED_BLOCK) && !BedwarsDataManagement.isMaterialAllowed(e.getBlock()))
			{	
				e.getPlayer().sendMessage(ChatColor.RED+"You can't break a part of the arena besides the enemy teams bed and"+BedwarsDataManagement.getAllowedMaterials()+"!");
				e.setCancelled(true);
			}
			
		}	
	}	
		private void onBedBreak(BlockBreakEvent e, List<String> bwd, BedwarsArena bwa)
		{
			Bed bed = (Bed) e.getBlock().getState().getData();
			Location loc = e.getBlock().getLocation();
			System.out.println(bwd.get(1));
			if (bwa.getTeamNumberFromBedLocation(loc) == Integer.valueOf(bwd.get(1)))
			{
				e.getPlayer().sendMessage(ChatColor.RED+"You can't break your own bed.");
				//System.out.println("lulz");
				e.setCancelled(true);
			}
			else 
			{
				//System.out.println("lulz");
				if (UtilityFunctions.getTwinLocation(bed, loc.getBlock()).getBlock().getType().equals(Material.BED_BLOCK))
				{
					System.out.println("lulz");
					RuntimeGamesManagement.getActiveGame(bwa.getName()).onValidBedBreak(loc);	
					e.getBlock().getDrops().clear();
				}
			}
			System.out.println("THIS IS A TEST");
		}
	
	@EventHandler
	public void onArenaPlace(BlockPlaceEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getPlayer());
		System.out.println("Placed material is of type "+e.getBlock().getType() + " allowed materials = "+ BedwarsDataManagement.getAllowedMaterials());
		if (bwd != null)
		{
			if (!BedwarsDataManagement.isMaterialAllowed(e.getBlockPlaced()))
			{
				e.setCancelled(true);
			}			
			else
			{
				BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation(e.getBlock().getLocation());
				if (bwa == null)
				{
					e.setCancelled(true);
				}
			}
		}
		
	}
	
	@EventHandler 
	public void onServerLeave(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		List<String> bwd = BedwarsDataManagement.getActiveGameData(player);
		if (bwd != null)
		{
			e.getPlayer().teleport(PlayerDataManagement.getPrePortLocation(player));
			RuntimeGamesManagement.removePlayerFromSystem(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onBastardCheat(PlayerGameModeChangeEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getPlayer());
		if (bwd != null && !e.getNewGameMode().equals(GameMode.SURVIVAL))
		{
			ActiveGame ag = RuntimeGamesManagement.getActiveGame(bwd.get(0));
			ag.removePlayer(e.getPlayer(), true);
			e.getPlayer().sendMessage(ChatColor.RED+"Get out of here.. scum!");
			Location loc = PlayerDataManagement.getAndClearOneTimeRespawnLocation(e.getPlayer());
			e.getPlayer().teleport(loc != null ? loc : ServerDataManagement.getServerSpawnPoint());
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getPlayer().getLocation());
		if (bwa != null)
		{
			Location loc = PlayerDataManagement.getAndClearOneTimeRespawnLocation(e.getPlayer());
			e.getPlayer().teleport(loc != null ? loc : ServerDataManagement.getServerSpawnPoint());
		}
	}
}
