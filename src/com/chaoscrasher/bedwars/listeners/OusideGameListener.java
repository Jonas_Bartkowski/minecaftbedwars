package com.chaoscrasher.bedwars.listeners;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.material.Bed;

import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.io.BedwarsDataManagement;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;

import net.md_5.bungee.api.ChatColor;

public class OusideGameListener implements Listener
{
	/*
	@EventHandler
	public void onLobbySignInteract(PlayerInteractEvent e)
	{
		
	}
	*/
	
	@EventHandler
	public void onBreakBlockInArena(BlockBreakEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getPlayer());
		if (bwd == null)
		{
			if (!e.getPlayer().isOp())
			{
				BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getBlock().getLocation());
				if ((bwa != null))
				{
					e.setCancelled(true);		
					e.getPlayer().sendMessage(ChatColor.RED+"You need to be OP to modify an arena.");
				}
			}
			else if (e.getBlock().getType().equals(Material.BED_BLOCK))
			{
				Bed bed = (Bed) e.getBlock().getState().getData();
				BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getBlock().getLocation());
				if ((bwa != null))
				{
					e.setCancelled(true);		
					e.getPlayer().sendMessage(ChatColor.RED+"You need to use /bwa setbloc to reset the position of the beds.");
				}
			}
		}
	}
	
	@EventHandler
	public void onPlaceBlockInArena(BlockPlaceEvent e)
	{
		List<String> bwd = BedwarsDataManagement.getActiveGameData(e.getPlayer());
		if (bwd == null)
		{
			if (!e.getPlayer().isOp())
			{
				BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getBlock().getLocation());
				
				if (bwa != null)
				{
					e.getPlayer().sendMessage(ChatColor.RED+"You need to be OP to modify an arena.");
					e.setCancelled(true);
				}
			}
			else if (e.getBlock().getType().equals(Material.BED_BLOCK))
			{
				BedwarsArena bwa = RuntimeGamesManagement.getArenaOfLocation_ignore_y(e.getBlock().getLocation());
				if ((bwa != null))
				{
					e.setCancelled(true);		
					e.getPlayer().sendMessage(ChatColor.RED+"You need to use /bwa setbloc to set the position of a bed.");
				}
			}
		}
	}
}
