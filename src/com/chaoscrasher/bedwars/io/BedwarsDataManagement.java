package com.chaoscrasher.bedwars.io;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.chaoscrasher.bedwars.data.BedwarsArena;
import com.chaoscrasher.bedwars.runtimemanagement.RuntimeGamesManagement;
import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;

public class BedwarsDataManagement 
{
	public static final String ACTIVE_GAME_KEY = "bedwarsdata";
	public static final String IN_QUEUE_KEY= "inqueue";
	public static final String GOT_KIT_KEY = "gotkit";
	public static final String EDIT_ARENA_KEY = "editArena";
	
	public static Material[] ALLOWED_MATERIALS = new Material[] {Material.SANDSTONE};
	
	public static void setBedwarsData(Player player, String arena, int team)
	{
		PlayerDataManagement.setStateData_overwrite(player, ACTIVE_GAME_KEY, arena, team+"");
	}
	
	public static void setPlayerInQueue(Player player)
	{
		PlayerDataManagement.setStateData_overwrite(player, IN_QUEUE_KEY, true+"");
	}
	
	/*
	public static boolean isPlayerInQueue(Player player)
	{
		List<String> dat = (PlayerDataManagement.getStateData(player, IN_QUEUE_KEY));
		return (dat != null && dat.get(0).equalsIgnoreCase("true"));
	}
	*/
	
	public static List<String> getActiveGameData(Player player)
	{
		List<String> bwd = PlayerDataManagement.getStateData(player, ACTIVE_GAME_KEY);
		if (bwd != null)
			return bwd;
		
		return null;
	}
	
	public static boolean isPayerInGame(Player player)
	{
		return getActiveGameData(player) != null;
	}
	
	public static void clearPlayerBedwarsData(Player player)
	{
		PlayerDataManagement.clearPlayerStateData(player, ACTIVE_GAME_KEY);
		PlayerDataManagement.clearPlayerStateData(player, GOT_KIT_KEY);
		PlayerDataManagement.clearPrePortLocation(player);
	}
	
	public static BedwarsArena getPlayersBedWarsArena(Player player)
	{
		List<String> bwd = getActiveGameData(player);
		if (bwd != null)
		{
			return RuntimeGamesManagement.getArenaFromAll(bwd.get(0));
		}
		return null;
	}
	
	public static void setPlayerGotKit(Player player, boolean gotkit)
	{
		PlayerDataManagement.setStateData_overwrite(player, GOT_KIT_KEY, gotkit+"");
	}
	
	public static boolean hasPlayerGottenKit(Player player)
	{
		List<String> gotKit = PlayerDataManagement.getStateData(player, GOT_KIT_KEY);
		if (gotKit != null)
		{
			return gotKit.get(0).equalsIgnoreCase("true");
		}
		return false;
	}
	
	private static void setPlayerEditArena(Player player, String aname)
	{
		PlayerDataManagement.setStateData_overwrite(player, EDIT_ARENA_KEY, aname);
	}
	
	public static boolean setPlayerEditArena_safe(Player player, String aname)
	{
		if (RuntimeGamesManagement.isArenaIntegratedIntoAll(aname))
		{
			setPlayerEditArena(player, aname);
			return true;
		}
		return false;
	}
	
	public static String getPlayerEditingArena(Player player)
	{
		return PlayerDataManagement.getFirstElementOfStateData(player, EDIT_ARENA_KEY);
	}
	
	public static BedwarsArena getPlayerEditingArena_safe(Player player)
	{
		String aname = getPlayerEditingArena(player);
		if (aname != null)
		{
			BedwarsArena bwa = RuntimeGamesManagement.getArenaFromAll(aname);
			return bwa;
		}
		return null;
	}
	
	public static boolean isMaterialAllowed(Material mat)
	{
		System.out.println("Testing if material "+mat+" is allowed.");
		for (Material material: ALLOWED_MATERIALS)
		{
			if (material.equals(mat))
			{
				System.out.println("Material allowed.");
				return true;
			}
		}
		System.out.println("Material now allowed");
		return false;
	}
	
	public static boolean isMaterialAllowed(Location loc)
	{
		return isMaterialAllowed(loc.getBlock().getType());
	}
	
	public static boolean isMaterialAllowed(Block block)
	{
		return isMaterialAllowed(block.getType());
	}
	
	public static String getAllowedMaterials()
	{
		String ret = "";
		for (Material mat: ALLOWED_MATERIALS)
		{
			ret += " "+mat;
		}
		return ret;
	}
}
