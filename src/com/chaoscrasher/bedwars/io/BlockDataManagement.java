package com.chaoscrasher.bedwars.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;

public class BlockDataManagement 
{
	private static HashMap<Location, List<String>> allBlockData = new HashMap<Location, List<String>>();
	
	public static List<String> getBlockData(Location loc)
	{
		return allBlockData.get(loc);
	}
	
	public static void putBlockData(Location loc, List<String> data)
	{
		allBlockData.put(loc, data);
	}
	
	public static void putBlockData(Location loc, String... data)
	{
		List<String> pData = getBlockData(loc);
		if (pData == null)
		{
			pData = new ArrayList();
		}
		
		for (String dp: data)
		{
			pData.add(dp);
		}
		
		if (pData.size() == data.length) {putBlockData(loc, data);}
	}
	
	public static void addToBlockData(Location loc, List<String> data)
	{
		List<String> prevDat = allBlockData.get(loc);
		
		if (prevDat != null)
			putBlockData(loc, data);
		else
			prevDat.addAll(data);
	}
	
	public static void addToBlockData(Location loc, String... data)
	{
		List<String> prevDat = allBlockData.get(loc);
		
		if (prevDat == null)
			putBlockData(loc, data);
		else
		{
			for (String dp: data)
			{
				prevDat.add(dp);
			}
		}
	}
	
	public static boolean containsBlockData(Location loc, String... data)
	{
		List<String> prevDat = getBlockData(loc);
		if (prevDat == null)
		{
			return false;
		}
		
		for (String dp: data)
		{
			if (!prevDat.contains(dp))
				return false;
		}
		return true;
	}
	
	public static boolean containsBlockData(Location loc, List<String> data)
	{
		List<String> prevDat = getBlockData(loc);
		if (prevDat == null)
		{
			return false;
		}
		return prevDat.containsAll(data);
	}
	
	public static void clearBlockData(Location loc)
	{
		allBlockData.remove(loc);
	}
}
